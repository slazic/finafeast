package com.example.FinaFeast.dao.implementation;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;

import com.example.FinaFeast.dao.OrderDao;
import com.example.FinaFeast.model.Order;
import com.example.FinaFeast.model.Restaurant;
import com.example.FinaFeast.model.User;

@Repository
public class OrderDaoImpl implements OrderDao{
	
	private JdbcTemplate jdbcTemplate;
	private TransactionTemplate transactionTemplate;

	public OrderDaoImpl(JdbcTemplate jdbcTemplate, TransactionTemplate transactionTemplate) {
		this.jdbcTemplate = jdbcTemplate;
		this.transactionTemplate = transactionTemplate;
	}

	private static Logger logger = LoggerFactory.getLogger(DishDaoImpl.class);
	
	@Override
	public List<Order> findById(final Long id) {
		final String query = 
				"select o.id, o.date_from, o.date_to,\r\n"
				+ "u.id,\r\n"
				+ "u.first_name,\r\n"
				+ "u.last_name,\r\n"
				+ "u.phone,\r\n"
				+ "u.address,\r\n"
				+ "u.email,\r\n"
				+ "r.id, r.name, r.address,\r\n"
				+ "m.id,\r\n"
				+ "m.first_name,\r\n"
				+ "m.last_name,\r\n"
				+ "m.phone,\r\n"
				+ "m.address,\r\n"
				+ "m.email,\r\n"
				+ "s.status\r\n"
				+ "from order_status os \r\n"
				+ "inner join status s on os.status_id = s.id\r\n"
				+ "inner join \"order\" o on os.order_id = o.id\r\n"
				+ "inner join restaurant r on o.restaurant_id = r.id\r\n"
				+ "inner join \"user\" u on o.user_id = u.id\r\n"
				+ "inner join \"user\" m on r.manager_id = m.id\r\n"
				+ "where o.id = ?;";
		final String selectDelivererQuery = 
				"select d.id\r\n"
				+ "d.first_name,\r\n"
				+ "d.last_name,\r\n"
				+ "d.phone,\r\n"
				+ "d.address,\r\n"
				+ "d.email,\r\n"
				+ "from \"order\" o\r\n"
				+ "inner join \"user\" d on o.deliverer_id = d.id\r\n"
				+ "where o.id = ?;";
		
		List<Order> orders = new ArrayList<>();
		try {
			orders = jdbcTemplate.query(
					query, 
					new Object[] {id}, 
					new int[] {Types.BIGINT}, 
					new RowMapper<>() {

						@Override
						public Order mapRow(ResultSet rs, int rowNum) throws SQLException {
							Order order = new Order();
							User user = new User();
							User deliverer = new User();
							User manager = new User();
							Restaurant restaurant = new Restaurant();
							
							order.setId(rs.getLong(1));
							order.setDateFrom(rs.getTimestamp(2));
							order.setDateTo(rs.getTimestamp(3));
							
							user.setId(rs.getLong(4));
							user.setFirstName(rs.getString(5));
							user.setLastName(rs.getString(6));
							user.setPhone(rs.getString(7));
							user.setAddress(rs.getString(8));
							user.setEmail(rs.getString(9));

							restaurant.setId(rs.getLong(10));
							restaurant.setName(rs.getString(11));
							restaurant.setAddress(rs.getString(12));
							
							manager.setId(rs.getLong(13));
							manager.setFirstName(rs.getString(14));
							manager.setLastName(rs.getString(15));
							manager.setPhone(rs.getString(16));
							manager.setAddress(rs.getString(17));
							manager.setEmail(rs.getString(18));
							
							restaurant.setManager(manager);
							
							order.setStatus(rs.getString(19));
							
							try {
								deliverer = jdbcTemplate.query(selectDelivererQuery, new Object[] {id}, new int[] {Types.BIGINT}, new ResultSetExtractor<User> () {

											@Override
											public User extractData(ResultSet rs)
													throws SQLException, DataAccessException {
												
												User deliverer = new User();
												
												while(rs.next()) {
													deliverer.setId(rs.getLong(1));
													deliverer.setFirstName(rs.getString(2));
													deliverer.setLastName(rs.getString(3));
													deliverer.setPhone(rs.getString(4));
													deliverer.setAddress(rs.getString(5));
													deliverer.setEmail(rs.getString(6));
												}
												return deliverer;
											}
									
								});
							} catch(Exception exception) {
								deliverer = null;
							}
												
							order.setUser(user);
							order.setDeliverer(deliverer);
							order.setRestaurant(restaurant);
							
							return order;
					}
			});

			if(orders.isEmpty()) {
				logger.info("Orders not found.");
				throw new NoSuchElementException("Orders not found.");
			}
		} catch(DataRetrievalFailureException exception) {
			logger.error("Unable to map.");
		} catch(DataAccessException exception) {
			logger.error("Error from database: " + exception + "\n");
		}
		
		return orders;
	}

	@Override
	public List<Order> findAll() {
		final String selectAllOrdersQuery = 
				"select o.id, o.date_from, o.date_to,\r\n"
				+ "u.id,\r\n"
				+ "u.first_name,\r\n"
				+ "u.last_name,\r\n"
				+ "u.phone,\r\n"
				+ "u.address,\r\n"
				+ "u.email,\r\n"
				+ "r.id, r.name, r.phone, r.address,\r\n"
				+ "m.id,\r\n"
				+ "m.first_name,\r\n"
				+ "m.last_name,\r\n"
				+ "m.phone,\r\n"
				+ "m.address,\r\n"
				+ "m.email,\r\n"
				+ "s.status\r\n"
				+ "from order_status os \r\n"
				+ "inner join status s on os.status_id = s.id\r\n"
				+ "inner join \"order\" o on os.order_id = o.id\r\n"
				+ "inner join restaurant r on o.restaurant_id = r.id\r\n"
				+ "inner join \"user\" u on o.user_id = u.id\r\n"
				+ "inner join \"user\" m on r.manager_id = m.id\r\n"
				+ "where os.active = true;";
		final String selectDelivererQuery = 
				"select d.id,\r\n"
				+ "d.first_name,\r\n"
				+ "d.last_name,\r\n"
				+ "d.phone,\r\n"
				+ "d.address,\r\n"
				+ "d.email\r\n"
				+ "from \"order\" o\r\n"
				+ "inner join \"user\" d on o.deliverer_id = d.id\r\n"
				+ "where o.id = ?;";
		
		List<Order> orders = new ArrayList<>();
		
		try {
			orders = jdbcTemplate.query(selectAllOrdersQuery, new RowMapper<Order>() {

				@Override
				public Order mapRow(ResultSet rs, int rowNum) throws SQLException {
					Order order = new Order();
					User user = new User();
					User deliverer = new User();
					User manager = new User();
					Restaurant restaurant = new Restaurant();
										
					order.setId(rs.getLong(1));
					order.setDateFrom(rs.getTimestamp(2));
					order.setDateTo(rs.getTimestamp(3));
					
					user.setId(rs.getLong(4));
					user.setFirstName(rs.getString(5));
					user.setLastName(rs.getString(6));
					user.setPhone(rs.getString(7));
					user.setAddress(rs.getString(8));
					user.setEmail(rs.getString(9));

					restaurant.setId(rs.getLong(10));
					restaurant.setName(rs.getString(11));
					restaurant.setPhone(rs.getString(12));
					restaurant.setAddress(rs.getString(13));
					
					manager.setId(rs.getLong(14));
					manager.setFirstName(rs.getString(15));
					manager.setLastName(rs.getString(16));
					manager.setPhone(rs.getString(17));
					manager.setAddress(rs.getString(18));
					manager.setEmail(rs.getString(19));
					
					restaurant.setManager(manager);
					
					order.setStatus(rs.getString(20));
					
					try {
						deliverer = jdbcTemplate.query(selectDelivererQuery, new Object[] {rs.getLong(1)}, new int[] {Types.BIGINT}, new ResultSetExtractor<User> () {

							@Override
							public User extractData(ResultSet rs)
									throws SQLException, DataAccessException {
								
								User deliverer = new User();
								
								while(rs.next()) {
									deliverer.setId(rs.getLong(1));
									deliverer.setFirstName(rs.getString(2));
									deliverer.setLastName(rs.getString(3));
									deliverer.setPhone(rs.getString(4));
									deliverer.setAddress(rs.getString(5));
									deliverer.setEmail(rs.getString(6));
								}
								return deliverer;
							}
					
						});
						if(deliverer.getId() == null) {
							logger.info("Deliverer not found.");
							throw new NoSuchElementException("Deliverer not found.");
						}
					} catch(NoSuchElementException exception) {
						// ignore exception
					}
										
					order.setUser(user);
					order.setDeliverer(deliverer);
					order.setRestaurant(restaurant);
					
					return order;
				}
				
			});
			
			if(orders.isEmpty()) {
				logger.info("Orders not found.");
				throw new NoSuchElementException("Orders not found.");
			}
		} catch(DataRetrievalFailureException exception) {
			logger.error("Unable to map.");
		} catch(DataAccessException exception) {
			logger.error("Error from database: " + exception + "\n");
		}
		
		return orders;
	}

	@Override
	public void save(final Order order, final List<Long> selectedDishesIds) {
		transactionTemplate.execute(new TransactionCallback<Integer>() {

			@Override
			public Integer doInTransaction(TransactionStatus status) {
				try {
					//mogući nedostatak je što vrijeme date_from neće biti isto za order i za status CREATED, query kasni jedan za drugim
					final String createOrderQuery = 
							"INSERT INTO \"order\" (date_from, user_id, restaurant_id) values (current_timestamp, ?, ?)";
					
					final String setOrderedDishQuery = 
							"INSERT INTO order_dish (order_id, dish_id) values (?, ?)";
					
					final String setCreatedStatusQuery = 
							"INSERT INTO order_status (order_id, status_id, date_from, active) values (?, 1, current_timestamp, true)";
					
					KeyHolder keyHolder = new GeneratedKeyHolder();

					jdbcTemplate.update(new PreparedStatementCreator() {

						@Override
						public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
							PreparedStatement statement = connection.prepareStatement(
																		createOrderQuery,
																		Statement.RETURN_GENERATED_KEYS
																	 );			
							
							statement.setObject(1, order.getUser().getId(), Types.BIGINT);
							statement.setObject(2, order.getRestaurant().getId(), Types.BIGINT);
							
							return statement;
						}
						
					}, keyHolder);
					
					final Long generatedOrderId = (Long) keyHolder.getKeys().get("id");
					
					for(final Long selectedDishId : selectedDishesIds) {
						jdbcTemplate.update(new PreparedStatementCreator() {

							@Override
							public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
								PreparedStatement statement = connection.prepareStatement(setOrderedDishQuery);
								
								statement.setObject(1, generatedOrderId, Types.BIGINT);
								statement.setObject(2, selectedDishId, Types.BIGINT);
								
								return statement;
							}
							
						});
					}
					
					jdbcTemplate.update(new PreparedStatementCreator() {

						@Override
						public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
							PreparedStatement statement = connection.prepareStatement(setCreatedStatusQuery);
							
							statement.setObject(1, generatedOrderId, Types.BIGINT);
							
							return statement;
						}
						
					});

					
					
				} catch (Exception exception) {
					logger.info("Error from database: ", exception);
					status.setRollbackOnly();
				}
				return null;
			}
			
		});
	}

	@Override
	public void update(Order t) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(final Long id) {
		final String query = "DELETE FROM \"order\" WHERE id = ?;";
		try {
			final int updatedCount = jdbcTemplate.update(query, id);
			if(updatedCount == 0) {
				logger.info("Order not deleted.");
				throw new NoSuchElementException("Order not deleted.");
			}
		} catch (DataAccessException exception) {
			logger.error("Error from database: " + exception + "\n");
		}
	}

	@Override
	public void updateStatus(final Long orderId) {
		transactionTemplate.execute(new TransactionCallback<Integer>() {

			@Override
			public Integer doInTransaction(TransactionStatus status) {
				
				try {
					final String selectCurrentStatusIdQuery = 
							"SELECT status_id FROM order_status WHERE order_id = ? AND active = true AND date_to IS NULL";
					
					final String setActiveToFalseQuery = 
							"UPDATE order_status \r\n"
							+ "SET date_to = current_timestamp, active = false \r\n"
							+ "WHERE order_id = ? AND active = true AND date_to IS NULL";
					
					final String insertNewOrderStatusQuery = 
							"INSERT INTO order_status \r\n"
							+ "(order_id, status_id, date_from, active) \r\n"
							+ "VALUES (?, ?, current_timestamp, true)";
					
					final String setDeliveredTimestampQuery = 
							"UPDATE \"order\" SET date_to = current_timestamp WHERE id = ? AND date_to IS NULL;";
					
					Long currentStatusId = jdbcTemplate.queryForObject(
							selectCurrentStatusIdQuery, 
							new Object[] {orderId},
							new int[] {Types.BIGINT},
							Long.class
							);
					
					if(currentStatusId >= 5)
						throw new ArrayIndexOutOfBoundsException("Order is already delivered.");
					
					Long newStatusId = currentStatusId + 1l;
					
					int updatedCount = jdbcTemplate.update(
							setActiveToFalseQuery, 
							new Object[] {orderId}, 
							new int[] {Types.BIGINT}
					);
					
					updatedCount = jdbcTemplate.update(
							insertNewOrderStatusQuery, 
							new Object[] {orderId, newStatusId}, 
							new int [] {Types.BIGINT, Types.BIGINT}
					);
					
					if(newStatusId == 5) {
						updatedCount = jdbcTemplate.update(
								setDeliveredTimestampQuery,
								new Object[] {orderId},
								new int[] {Types.BIGINT}
						);
					}
					
					if(updatedCount == 0) {
						logger.info("Order status not updated.");
						throw new IllegalArgumentException("Order status not updated.");
					}
					
				} catch (ArrayIndexOutOfBoundsException exception) {
					status.setRollbackOnly();
					logger.info("Error from database: ", exception);
					throw new IllegalArgumentException(exception.getMessage(), exception);
				} catch (Exception exception) {
					status.setRollbackOnly();
					logger.info("Error from database: ", exception);
				}
				
				return null;
				
			}
			
		});
	}

	@Override
	public void setDeliverer(final Long orderId, final Long delivererId) {
		final String query = 
				"update \"order\" set deliverer_id = ? where id = ? and deliverer_id is null;";
		
		try {
			final int updatedCount = jdbcTemplate.update(new PreparedStatementCreator() {

				@Override
				public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
					PreparedStatement statement = connection.prepareStatement(query);
					
					statement.setObject(1, delivererId, Types.BIGINT);
					statement.setObject(2, orderId, Types.BIGINT);
					
					return statement;
				}
				
			});
			
			if(updatedCount == 0) {
				logger.info("Deliverer not set.");
				throw new IllegalArgumentException("Deliverer not set.");
			}
		} catch(Exception exception) {
			logger.info("Error from database: ", exception);
		}
	}

	@Override
	public List<Order> findAllOrdersPlacedInRestaurant(final Long managerId) {
		final String query = 
				"select o.id, o.date_from, o.date_to,\r\n"
				+ "u.id,\r\n"
				+ "u.first_name,\r\n"
				+ "u.last_name,\r\n"
				+ "u.phone,\r\n"
				+ "u.address,\r\n"
				+ "u.email,\r\n"
				+ "r.id, r.name, r.phone, r.address,\r\n"
				+ "m.id,\r\n"
				+ "m.first_name,\r\n"
				+ "m.last_name,\r\n"
				+ "m.phone,\r\n"
				+ "m.address,\r\n"
				+ "m.email,\r\n"
				+ "s.status\r\n"
				+ "from order_status os \r\n"
				+ "inner join status s on os.status_id = s.id\r\n"
				+ "inner join \"order\" o on os.order_id = o.id \r\n"
				+ "inner join \"user\" u on o.user_id = u.id\r\n"
				+ "inner join restaurant r on o.restaurant_id = r.id\r\n"
				+ "inner join \"user\" m on r.manager_id = m.id\r\n"
				+ "where os.active = true and m.id=?;";
		final String selectDelivererQuery = 
				"select d.id,\r\n"
				+ "d.first_name,\r\n"
				+ "d.last_name,\r\n"
				+ "d.phone,\r\n"
				+ "d.address,\r\n"
				+ "d.email\r\n"
				+ "from \"order\" o\r\n"
				+ "inner join \"user\" d on o.deliverer_id = d.id\r\n"
				+ "where o.id = ?;";
	
		List<Order> orders = new ArrayList<>();
		try {
			orders = jdbcTemplate.query(
					query,
					new Object[] {managerId},
					new int[] {Types.BIGINT},
					new RowMapper<>() {

						@Override
						public Order mapRow(ResultSet rs, int rowNum) throws SQLException {
							Order order = new Order();
							User user = new User();
							User deliverer = new User();
							User manager = new User();
							Restaurant restaurant = new Restaurant();
												
							order.setId(rs.getLong(1));
							order.setDateFrom(rs.getTimestamp(2));
							order.setDateTo(rs.getTimestamp(3));
							
							user.setId(rs.getLong(4));
							user.setFirstName(rs.getString(5));
							user.setLastName(rs.getString(6));
							user.setPhone(rs.getString(7));
							user.setAddress(rs.getString(8));
							user.setEmail(rs.getString(9));

							restaurant.setId(rs.getLong(10));
							restaurant.setName(rs.getString(11));
							restaurant.setPhone(rs.getString(12));
							restaurant.setAddress(rs.getString(13));
							
							manager.setId(rs.getLong(14));
							manager.setFirstName(rs.getString(15));
							manager.setLastName(rs.getString(16));
							manager.setPhone(rs.getString(17));
							manager.setAddress(rs.getString(18));
							manager.setEmail(rs.getString(19));
							
							restaurant.setManager(manager);
							
							order.setStatus(rs.getString(20));
							
							try {
								deliverer = jdbcTemplate.query(selectDelivererQuery, new Object[] {rs.getLong(1)}, new int[] {Types.BIGINT}, new ResultSetExtractor<User> () {

									@Override
									public User extractData(ResultSet rs)
											throws SQLException, DataAccessException {
										
										User deliverer = new User();
										
										while(rs.next()) {
											deliverer.setId(rs.getLong(1));
											deliverer.setFirstName(rs.getString(2));
											deliverer.setLastName(rs.getString(3));
											deliverer.setPhone(rs.getString(4));
											deliverer.setAddress(rs.getString(5));
											deliverer.setEmail(rs.getString(6));
										}
										return deliverer;
									}
							
								});
								if(deliverer.getId() == null) {
									logger.info("Deliverer not found.");
									throw new NoSuchElementException("Deliverer not found.");
								}
							} catch(NoSuchElementException exception) {
								// ignore exception
							}
												
							order.setUser(user);
							order.setDeliverer(deliverer);
							order.setRestaurant(restaurant);
							
							return order;
						}
						
					});
			
			if(orders.isEmpty()) {
				logger.info("Orders not found.");
				throw new NoSuchElementException("Orders not found.");
			}
		} catch(DataRetrievalFailureException exception) {
			logger.error("Unable to map.");
		} catch(DataAccessException exception) {
			logger.error("Error from database: " + exception + "\n");
		}
		
		return orders;
	}
	
	@Override
	public List<Order> findAllOrdersPlacedInRestaurantByStatus(Long managerId, String status) {
		final String query = 
				"select o.id, o.date_from,\r\n"
				+ "u.id, u.first_name, u.last_name, u.phone,\r\n"
				+ "s.status\r\n"
				+ "from order_status os \r\n"
				+ "inner join status s on os.status_id = s.id\r\n"
				+ "inner join \"order\" o on os.order_id = o.id \r\n"
				+ "inner join \"user\" u on o.user_id = u.id\r\n"
				+ "inner join restaurant r on o.restaurant_id = r.id\r\n"
				+ "inner join \"user\" m on r.manager_id = m.id\r\n"
				+ "where os.active = true and m.id = ? and s.status = ?;";
		List<Order> orders = new ArrayList<>();
		try {
			orders = jdbcTemplate.query(
					query,
					new Object[] {managerId, status},
					new int[] {Types.BIGINT, Types.VARCHAR},
					new RowMapper<>() {

						@Override
						public Order mapRow(ResultSet rs, int rowNum) throws SQLException {
							Order order = new Order();
							User user = new User();
							
							order.setId(rs.getLong(1));
							order.setDateFrom(rs.getTimestamp(2));
							
							user.setId(rs.getLong(3));
							user.setFirstName(rs.getString(4));
							user.setLastName(rs.getString(5));
							user.setPhone(rs.getString(6));
							
							order.setStatus(rs.getString(7));
							order.setUser(user);
							
							return order;
						}
						
					});
			
			if(orders.isEmpty()) {
				logger.info("Orders not found.");
				throw new NoSuchElementException("Orders not found.");
			}
		} catch(DataRetrievalFailureException exception) {
			logger.error("Unable to map.");
		} catch(DataAccessException exception) {
			logger.error("Error from database: " + exception + "\n");
		}
		
		return orders;
	}
	
	@Override
	public List<Order> findAllOrdersAvailableForDelivery() {
		final String query = 
				"select o.id, o.date_from, o.date_to,\r\n"
				+ "u.id,\r\n"
				+ "u.first_name,\r\n"
				+ "u.last_name,\r\n"
				+ "u.phone,\r\n"
				+ "u.address,\r\n"
				+ "u.email,\r\n"
				+ "r.id, r.name, r.phone, r.address,\r\n"
				+ "m.id,\r\n"
				+ "m.first_name,\r\n"
				+ "m.last_name,\r\n"
				+ "m.phone,\r\n"
				+ "m.address,\r\n"
				+ "m.email,\r\n"
				+ "s.status\r\n"
				+ "from order_status os\r\n"
				+ "inner join status s on os.status_id = s.id\r\n"
				+ "inner join \"order\" o on os.order_id = o.id\r\n"
				+ "inner join \"user\" u on o.user_id = u.id\r\n"
				+ "inner join restaurant r on o.restaurant_id = r.id\r\n"
				+ "inner join \"user\" m on r.manager_id = m.id\r\n"
				+ "where s.status = 'PREPARING' and os.active = true and o.deliverer_id is null or s.status = 'PREPARED' and os.active = true and o.deliverer_id  is null;";

		List<Order> orders = new ArrayList<>();
		
		try {
			
			orders = jdbcTemplate.query(query, new RowMapper<Order>() {

				@Override
				public Order mapRow(ResultSet rs, int rowNum) throws SQLException {
					Order order = new Order();
					User user = new User();
					User deliverer = new User();
					User manager = new User();
					Restaurant restaurant = new Restaurant();
										
					order.setId(rs.getLong(1));
					order.setDateFrom(rs.getTimestamp(2));
					order.setDateTo(rs.getTimestamp(3));
					
					user.setId(rs.getLong(4));
					user.setFirstName(rs.getString(5));
					user.setLastName(rs.getString(6));
					user.setPhone(rs.getString(7));
					user.setAddress(rs.getString(8));
					user.setEmail(rs.getString(9));

					restaurant.setId(rs.getLong(10));
					restaurant.setName(rs.getString(11));
					restaurant.setPhone(rs.getString(12));
					restaurant.setAddress(rs.getString(13));
					
					manager.setId(rs.getLong(14));
					manager.setFirstName(rs.getString(15));
					manager.setLastName(rs.getString(16));
					manager.setPhone(rs.getString(17));
					manager.setAddress(rs.getString(18));
					manager.setEmail(rs.getString(19));
					
					restaurant.setManager(manager);
					
					order.setStatus(rs.getString(20));
									
					order.setUser(user);
					order.setDeliverer(deliverer);
					order.setRestaurant(restaurant);
					
					return order;
				}
				
			});
			
			if(orders.isEmpty()) {
				logger.info("Orders not found.");
				throw new NoSuchElementException("Orders not found.");
			}
		} catch(DataRetrievalFailureException exception) {
			logger.error("Unable to map.");
		} catch(DataAccessException exception) {
			logger.error("Error from database: " + exception + "\n");
		}
		
		return orders;
	}
	
	@Override
	public List<Order> findAllOrdersByDeliverer(final Long delivererId) {
		final String query = 
				"select o.id, o.date_from, o.date_to,\r\n"
				+ "u.id,\r\n"
				+ "u.first_name,\r\n"
				+ "u.last_name,\r\n"
				+ "u.phone,\r\n"
				+ "u.address,\r\n"
				+ "u.email,\r\n"
				+ "r.id, r.name, r.phone, r.address,\r\n"
				+ "m.id,\r\n"
				+ "m.first_name,\r\n"
				+ "m.last_name,\r\n"
				+ "m.phone,\r\n"
				+ "m.address,\r\n"
				+ "m.email,\r\n"
				+ "s.status\r\n"
				+ "from order_status os\r\n"
				+ "inner join status s on os.status_id = s.id \r\n"
				+ "inner join \"order\" o on os.order_id = o.id \r\n"
				+ "inner join \"user\" u on o.user_id = u.id\r\n"
				+ "inner join restaurant r on o.restaurant_id = r.id\r\n"
				+ "inner join \"user\" m on r.manager_id = m.id\r\n"
				+ "where os.active = true and o.deliverer_id = ?;";
		final String selectDelivererQuery = 
				"select d.id,\r\n"
				+ "d.first_name,\r\n"
				+ "d.last_name,\r\n"
				+ "d.phone,\r\n"
				+ "d.address,\r\n"
				+ "d.email\r\n"
				+ "from \"order\" o\r\n"
				+ "inner join \"user\" d on o.deliverer_id = d.id\r\n"
				+ "where o.id = ?;";
		List<Order> orders = new ArrayList<>();
		
		try {
			
			orders = jdbcTemplate.query(
					query,
					new Object[] {delivererId},
					new int[] {Types.BIGINT},
					new RowMapper<Order>() {

				@Override
				public Order mapRow(ResultSet rs, int rowNum) throws SQLException {
					Order order = new Order();
					User user = new User();
					User deliverer = new User();
					User manager = new User();
					Restaurant restaurant = new Restaurant();
										
					order.setId(rs.getLong(1));
					order.setDateFrom(rs.getTimestamp(2));
					order.setDateTo(rs.getTimestamp(3));
					
					user.setId(rs.getLong(4));
					user.setFirstName(rs.getString(5));
					user.setLastName(rs.getString(6));
					user.setPhone(rs.getString(7));
					user.setAddress(rs.getString(8));
					user.setEmail(rs.getString(9));

					restaurant.setId(rs.getLong(10));
					restaurant.setName(rs.getString(11));
					restaurant.setPhone(rs.getString(12));
					restaurant.setAddress(rs.getString(13));
					
					manager.setId(rs.getLong(14));
					manager.setFirstName(rs.getString(15));
					manager.setLastName(rs.getString(16));
					manager.setPhone(rs.getString(17));
					manager.setAddress(rs.getString(18));
					manager.setEmail(rs.getString(19));
					
					restaurant.setManager(manager);
					
					order.setStatus(rs.getString(20));
					
					try {
						deliverer = jdbcTemplate.query(selectDelivererQuery, new Object[] {rs.getLong(1)}, new int[] {Types.BIGINT}, new ResultSetExtractor<User> () {

							@Override
							public User extractData(ResultSet rs)
									throws SQLException, DataAccessException {
								
								User deliverer = new User();
								
								while(rs.next()) {
									deliverer.setId(rs.getLong(1));
									deliverer.setFirstName(rs.getString(2));
									deliverer.setLastName(rs.getString(3));
									deliverer.setPhone(rs.getString(4));
									deliverer.setAddress(rs.getString(5));
									deliverer.setEmail(rs.getString(6));
								}
								return deliverer;
							}
					
						});
						if(deliverer.getId() == null) {
							logger.info("Deliverer not found.");
							throw new NoSuchElementException("Deliverer not found.");
						}
					} catch(NoSuchElementException exception) {
						// ignore exception
					}
										
					order.setUser(user);
					order.setDeliverer(deliverer);
					order.setRestaurant(restaurant);
					
					return order;
				}
				
			});
			
			if(orders.isEmpty()) {
				logger.info("Orders not found.");
				throw new NoSuchElementException("Orders not found.");
			}
		} catch(DataRetrievalFailureException exception) {
			logger.error("Unable to map.");
		} catch(DataAccessException exception) {
			logger.error("Error from database: " + exception + "\n");
		}
		
		return orders;
	}

	@Override
	public List<Order> findAllOrdersByUser(final Long userId) {
		final String selectAllOrdersQuery = 
				"select o.id, o.date_from, o.date_to,\r\n"
				+ "u.id,\r\n"
				+ "u.first_name,\r\n"
				+ "u.last_name,\r\n"
				+ "u.phone,\r\n"
				+ "u.address,\r\n"
				+ "u.email,\r\n"
				+ "r.id, r.name, r.phone, r.address,\r\n"
				+ "m.id,\r\n"
				+ "m.first_name,\r\n"
				+ "m.last_name,\r\n"
				+ "m.phone,\r\n"
				+ "m.address,\r\n"
				+ "m.email,\r\n"
				+ "s.status\r\n"
				+ "from order_status os\r\n"
				+ "inner join status s on os.status_id = s.id\r\n"
				+ "inner join \"order\" o on os.order_id = o.id\r\n"
				+ "inner join \"user\" u on o.user_id = u.id\r\n"
				+ "inner join restaurant r on o.restaurant_id = r.id\r\n"
				+ "inner join \"user\" m on r.manager_id = m.id\r\n"
				+ "where os.active = true and u.id = ?;";
		final String selectDelivererQuery = 
				"select d.id,\r\n"
				+ "d.first_name,\r\n"
				+ "d.last_name,\r\n"
				+ "d.phone,\r\n"
				+ "d.address,\r\n"
				+ "d.email\r\n"
				+ "from \"order\" o\r\n"
				+ "inner join \"user\" d on o.deliverer_id = d.id\r\n"
				+ "where o.id = ?;";
		List<Order> orders = new ArrayList<>();
		
		try {
			orders = jdbcTemplate.query(
					selectAllOrdersQuery,
					new Object[] {userId},
					new int[] {Types.BIGINT},
					new RowMapper<Order>() {

				@Override
				public Order mapRow(ResultSet rs, int rowNum) throws SQLException {
					Order order = new Order();
					User user = new User();
					User deliverer = new User();
					User manager = new User();
					Restaurant restaurant = new Restaurant();
										
					order.setId(rs.getLong(1));
					order.setDateFrom(rs.getTimestamp(2));
					order.setDateTo(rs.getTimestamp(3));
					
					user.setId(rs.getLong(4));
					user.setFirstName(rs.getString(5));
					user.setLastName(rs.getString(6));
					user.setPhone(rs.getString(7));
					user.setAddress(rs.getString(8));
					user.setEmail(rs.getString(9));

					restaurant.setId(rs.getLong(10));
					restaurant.setName(rs.getString(11));
					restaurant.setPhone(rs.getString(12));
					restaurant.setAddress(rs.getString(13));
					
					manager.setId(rs.getLong(14));
					manager.setFirstName(rs.getString(15));
					manager.setLastName(rs.getString(16));
					manager.setPhone(rs.getString(17));
					manager.setAddress(rs.getString(18));
					manager.setEmail(rs.getString(19));
					
					restaurant.setManager(manager);
					
					order.setStatus(rs.getString(20));
					
					try {
						deliverer = jdbcTemplate.query(selectDelivererQuery, new Object[] {rs.getLong(1)}, new int[] {Types.BIGINT}, new ResultSetExtractor<User> () {

							@Override
							public User extractData(ResultSet rs)
									throws SQLException, DataAccessException {
								
								User deliverer = new User();
								
								while(rs.next()) {
									deliverer.setId(rs.getLong(1));
									deliverer.setFirstName(rs.getString(2));
									deliverer.setLastName(rs.getString(3));
									deliverer.setPhone(rs.getString(4));
									deliverer.setAddress(rs.getString(5));
									deliverer.setEmail(rs.getString(6));
								}
								return deliverer;
							}
					
						});
						if(deliverer.getId() == null) {
							logger.info("Deliverer not found.");
							throw new NoSuchElementException("Deliverer not found.");
						}
					} catch(NoSuchElementException exception) {
						// ignore exception
					}
										
					order.setUser(user);
					order.setDeliverer(deliverer);
					order.setRestaurant(restaurant);
					
					return order;
				}
				
			});
			
			if(orders.isEmpty()) {
				logger.info("Orders not found.");
				throw new NoSuchElementException("Orders not found.");
			}
		} catch(DataRetrievalFailureException exception) {
			logger.error("Unable to map.");
		} catch(DataAccessException exception) {
			logger.error("Error from database: " + exception + "\n");
		}
		
		return orders;
	}
}
