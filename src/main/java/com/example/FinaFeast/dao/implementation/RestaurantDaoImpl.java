package com.example.FinaFeast.dao.implementation;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.example.FinaFeast.dao.RestaurantDao;
import com.example.FinaFeast.model.Restaurant;
import com.example.FinaFeast.model.User;

@Repository
public class RestaurantDaoImpl implements RestaurantDao {
	
	private JdbcTemplate jdbcTemplate;
	
	public RestaurantDaoImpl(
			final JdbcTemplate jdbcTemplate
	) {
		this.jdbcTemplate = jdbcTemplate;
	}
	
	private static Logger logger = LoggerFactory.getLogger(DishDaoImpl.class);
	
	@Override
	public Restaurant findById(Long id) {
		final String query = 
				"SELECT\r\n"
				+ "r.id,\r\n"
				+ "r.name,\r\n"
				+ "r.phone,\r\n"
				+ "r.address,\r\n"
				+ "u.id,\r\n"
				+ "u.first_name,\r\n"
				+ "u.last_name,\r\n"
				+ "u.phone,\r\n"
				+ "u.email\r\n"
				+ "from restaurant r\r\n"
				+ "inner join \"user\" u on r.manager_id = u.id where r.id = ?;";
		
		Restaurant restaurant = new Restaurant();
		try {
			restaurant = jdbcTemplate.query(query, new Object[] {id}, new int[] {Types.BIGINT}, new ResultSetExtractor<Restaurant>() {

				@Override
				public Restaurant extractData(ResultSet rs) throws SQLException, DataAccessException {
					Restaurant restaurant = new Restaurant();
					User user = new User();
					
					while(rs.next()) {
						if(restaurant.getId() == null) {
							restaurant.setId(rs.getLong(1));
							restaurant.setName(rs.getString(2));
							restaurant.setPhone(rs.getString(3));
							restaurant.setAddress(rs.getString(4));
							
							user.setId(rs.getLong(5));
							user.setFirstName(rs.getString(6));
							user.setLastName(rs.getString(7));
							user.setPhone(rs.getString(8));
							user.setEmail(rs.getString(9));
							
							restaurant.setManager(user);
						}
					}
					
					return restaurant;
				}
				
			});
			
			if(restaurant.getId() == null) {
				logger.info("Restaurant not found.");
				throw new NoSuchElementException("Restaurant not found.");
			}
		} catch(DataRetrievalFailureException exception) {
			logger.error("Unable to map.");
		} catch(DataAccessException exception) {
			logger.error("Error from database: " + exception + "\n");
		}
		
		return restaurant;
	}

	@Override
	public List<Restaurant> findAll() {
		final String query = 
				"SELECT\r\n"
				+ "r.id,\r\n"
				+ "r.name,\r\n"
				+ "r.phone,\r\n"
				+ "r.address,\r\n"
				+ "u.id,\r\n"
				+ "u.first_name,\r\n"
				+ "u.last_name,\r\n"
				+ "u.phone,\r\n"
				+ "u.email\r\n"
				+ "from restaurant r\r\n"
				+ "inner join \"user\" u on r.manager_id = u.id;";
		
		List<Restaurant> restaurants = new ArrayList<>();
		
		try {
			restaurants = jdbcTemplate.query(query, new RowMapper<Restaurant>() {

				@Override
				public Restaurant mapRow(ResultSet rs, int rowNum) throws SQLException {
					Restaurant restaurant = new Restaurant();
					User user = new User();
					
					restaurant.setId(rs.getLong(1));
					restaurant.setName(rs.getString(2));
					restaurant.setPhone(rs.getString(3));
					restaurant.setAddress(rs.getString(4));
					
					user.setId(rs.getLong(5));
					user.setFirstName(rs.getString(6));
					user.setLastName(rs.getString(7));
					user.setPhone(rs.getString(8));
					user.setEmail(rs.getString(9));
					
					restaurant.setManager(user);
					
					return restaurant;
				}
				
			});
			
			if(restaurants.isEmpty()) {
				logger.info("Restaurants not found.");
				throw new NoSuchElementException("Restaurants not found.");
			}
		} catch(DataRetrievalFailureException exception) {
			logger.error("Unable to map.");
		} catch(DataAccessException exception) {
			logger.error("Error from database: " + exception + "\n");
		}
		
		return restaurants;
	}

	@Override
	public void save(final Restaurant restaurant) {
		final String query = "INSERT INTO restaurant (name, phone, address, manager_id) VALUES (?, ?, ?, ?);";
		try {
			jdbcTemplate.update(new PreparedStatementCreator() {

				@Override
				public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
					PreparedStatement statement = connection.prepareStatement(query);
					
					statement.setObject(1, restaurant.getName(), Types.VARCHAR);
					statement.setObject(2, restaurant.getPhone(), Types.VARCHAR);
					statement.setObject(3, restaurant.getAddress(), Types.VARCHAR);
					statement.setObject(4, restaurant.getManager().getId(), Types.BIGINT);

					
					return statement;
				}
				
			});
		} catch(Exception exception) {
			logger.error("Error from database: " + exception + "\n");
		}
		
	}

	@Override
	public void update(final Restaurant restaurant) {
		final String query = "UPDATE restaurant SET name = ?, phone = ?, address = ?, manager_id = ? WHERE id = ?;";
		try {
			final int updatedCount = jdbcTemplate.update(new PreparedStatementCreator() {

				@Override
				public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
					PreparedStatement statement = connection.prepareStatement(query);
					
					statement.setObject(1, restaurant.getName(), Types.VARCHAR);
					statement.setObject(2, restaurant.getPhone(), Types.VARCHAR);
					statement.setObject(3, restaurant.getAddress(), Types.VARCHAR);
					statement.setObject(4, restaurant.getManager().getId(), Types.BIGINT);
					//WHERE
					statement.setObject(5, restaurant.getId(), Types.BIGINT);
					return statement;
				}
				
			});
			if(updatedCount == 0) {
				logger.info("Restaurant not updated.");
				throw new NoSuchElementException("Restaurant not updated.");
			}
		} catch (DataAccessException exception) {
			logger.error("Error from database: " + exception + "\n");
		}
	}

	@Override
	public void delete(Long id) {
		final String query = "DELETE FROM restaurant WHERE id = ?;";
		try {
			final int updatedCount = jdbcTemplate.update(query, id);
			if(updatedCount == 0) {
				logger.info("Restaurant not deleted.");
				throw new NoSuchElementException("Restaurant not deleted.");
			}
		} catch (DataAccessException exception) {
			logger.error("Error from database: " + exception + "\n");
		}
	}

}
