package com.example.FinaFeast.dao.implementation;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.example.FinaFeast.dao.DishDao;
import com.example.FinaFeast.model.Dish;

@Repository
public class DishDaoImpl implements DishDao {
	
	private JdbcTemplate jdbcTemplate;
	
	public DishDaoImpl(
			final JdbcTemplate jdbcTemplate
	) {
		this.jdbcTemplate = jdbcTemplate;
	}
	
	private static Logger logger = LoggerFactory.getLogger(DishDaoImpl.class);
	
	@Override
	public Dish findById(final Long id) {
		final String query = "SELECT * FROM dish WHERE id = ?;";
		Dish dish = new Dish();
		try {
			dish = jdbcTemplate.queryForObject(
					query, 
					new Object[] {id}, 
					new int[] {Types.BIGINT}, 
					new BeanPropertyRowMapper<>(Dish.class)
			);
		} catch(EmptyResultDataAccessException exception) {
			logger.info("Dish not found.");
			throw new NoSuchElementException("Dish not found.");
		} catch(DataRetrievalFailureException exception) {
			logger.error("Unable to map. Dish not found.");
		} catch(DataAccessException exception) {
			logger.error("Error from database: " + exception + "\n");
		}
		return dish;
	}

	@Override
	public List<Dish> findAll() {
		final String query = "SELECT * FROM dish;";
		List<Dish> dishes = new ArrayList<>();
		try {
			dishes = jdbcTemplate.query(query, new BeanPropertyRowMapper<>(Dish.class));
			if(dishes.isEmpty()) {
				logger.info("Dishes not found.");
				throw new NoSuchElementException("Dishes not found.");
			}
		} catch(DataRetrievalFailureException exception) {
			logger.error("Unable to map. Dishes not found.");
		} catch(DataAccessException exception) {
			logger.error("Error from database: " + exception + "\n");
		}
		
		return dishes;
	}

	@Override
	public void save(final Dish dish) {
		final String query = "INSERT INTO dish (dish_name, price, description) VALUES (?, ?, ?);";
		try {
			jdbcTemplate.update(new PreparedStatementCreator() {

				@Override
				public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
					PreparedStatement statement = connection.prepareStatement(query);
					
					statement.setObject(1, dish.getDishName(), Types.VARCHAR);
					statement.setObject(2, dish.getPrice(), Types.NUMERIC);
					statement.setObject(3, dish.getDescription(), Types.VARCHAR);
					
					return statement;
				}
				
			});
		} catch(Exception exception) {
			logger.error("Error from database: " + exception + "\n");
		}
	}

	@Override
	public void update(final Dish dish) {
		final String query = "UPDATE dish SET dish_name = ?, price = ?, description = ? WHERE id = ?;";
		try {
			final int updatedCount = jdbcTemplate.update(new PreparedStatementCreator() {

				@Override
				public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
					PreparedStatement statement = connection.prepareStatement(query);
					
					statement.setObject(1, dish.getDishName(), Types.VARCHAR);
					statement.setObject(2, dish.getPrice(), Types.NUMERIC);
					statement.setObject(3, dish.getDescription(), Types.VARCHAR);
					//WHERE
					statement.setObject(4, dish.getId(), Types.BIGINT);
					return statement;
				}
				
			});
			if(updatedCount == 0) {
				logger.info("Dish not updated.");
				throw new NoSuchElementException("Dish not updated.");
			}
		} catch (DataAccessException exception) {
			logger.error("Error from database: " + exception + "\n");
		}
	}

	@Override
	public void delete(final Long id) {
		final String query = "DELETE FROM dish WHERE id = ?;";
		try {
			final int updatedCount = jdbcTemplate.update(query, id);
			if(updatedCount == 0) {
				logger.info("Dish not deleted.");
				throw new NoSuchElementException("Dish not deleted.");
			}
		} catch (DataAccessException exception) {
			logger.error("Error from database: " + exception + "\n");
		}
	}

	@Override
	public List<Dish> findAllByOrderId(final Long orderId) {
		final String query = 
				"SELECT o.id, d.id, d.dish_name, d.price, d.description  \r\n"
				+ "FROM order_dish od \r\n"
				+ "INNER JOIN \"order\" o ON od.order_id = o.id\r\n"
				+ "INNER JOIN dish d ON od.dish_id = d.id where o.id = ?;";
		List<Dish> dishes = new ArrayList<>();
		
		try {
			dishes = jdbcTemplate.query(query, new Object[] {orderId}, new int[] {Types.BIGINT}, new RowMapper<Dish>() {

				@Override
				public Dish mapRow(ResultSet rs, int rowNum) throws SQLException {
					Dish dish = new Dish();
					
					dish.setId(rs.getLong(2));
					dish.setDishName(rs.getString(3));
					dish.setPrice(rs.getDouble(4));
					dish.setDescription(rs.getString(5));
					
					return dish;				
				}
				
			});
			
			if(dishes.isEmpty()) {
				logger.info("Dishes not found.");
				throw new NoSuchElementException("Dishes not found.");
			}
		} catch(DataRetrievalFailureException exception) {
			logger.error("Unable to map.");
		} catch(DataAccessException exception) {
			logger.error("Error from database: " + exception + "\n");
		}
		
		return dishes;
	}

}
