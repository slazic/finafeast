package com.example.FinaFeast.dao.implementation;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;

import com.example.FinaFeast.dao.UserDao;
import com.example.FinaFeast.model.Role;
import com.example.FinaFeast.model.User;

@Repository
public class UserDaoImpl implements UserDao{
	
	private JdbcTemplate jdbcTemplate;
	private TransactionTemplate transactionTemplate;
	
	public UserDaoImpl(
			final JdbcTemplate jdbcTemplate,
			final TransactionTemplate transactionTemplate
	) {
		this.jdbcTemplate = jdbcTemplate;
		this.transactionTemplate = transactionTemplate;
	}
	
	private static Logger logger = LoggerFactory.getLogger(DishDaoImpl.class);
	
	@Override
	public User findByUsername(final String email) {
		
		//for user details only the user name, password and roles are required
		final String query = 
				"SELECT DISTINCT \r\n"
				+ "u.id,\r\n"
				+ "u.email,\r\n"
				+ "u.password,\r\n"
				+ "r.id, \r\n"
				+ "r.name \r\n"
				+ "FROM user_role ur \r\n"
				+ "INNER JOIN \"user\" u ON u.id = ur.user_id AND ur.date_to IS NULL \r\n"
				+ "INNER JOIN \"role\" r ON r.id = ur.role_id \r\n"
				+ "WHERE u.email = ?;";
		User user = new User();
		try {
			user = jdbcTemplate.query(
					query, 
					new Object[]{email}, 
					new int[]{Types.VARCHAR}, 
					new ResultSetExtractor<User>() {
					
						@Override
						public User extractData(ResultSet rs) throws SQLException, DataAccessException {
							
							User user = new User();
							List<Role> roles = new ArrayList<>();
							
							while(rs.next()) {
								
								if(user.getId() == null) {
									user.setId(rs.getLong(1));
									user.setEmail(rs.getString(2));
									user.setPassword(rs.getString(3));
								}
								
								Role role = new Role(rs.getLong(4), rs.getString(5));
								switch(role.getName()) {
								case "USER":
									role.setName("ROLE_USER");
									break;
								case "ADMIN":
									role.setName("ROLE_ADMIN");
									break;
								case "DELIVERER":
									role.setName("ROLE_DELIVERER");
									break;
								case "MANAGER":
									role.setName("ROLE_MANAGER");
									break;
								default: break;
								}
								roles.add(role);
							}
							
							if(!roles.isEmpty())
								user.setRoles(roles);
							
							return user;
						}
						
					}
			);
			
			if(user.getId() == null) {
				logger.info("Username not found.");
				throw new UsernameNotFoundException("Username not found.");
			}
			
		} catch(DataRetrievalFailureException exception) {
			logger.error("Unable to map.");
		} catch(DataAccessException exception) {
			logger.error("Error from database: " + exception + "\n");
		}
		return user;
	}

	@Override
	public List<User> findAll() {
		final String query = "SELECT * FROM \"user\";";
		List<User> users = new ArrayList<>();
		try {
			users = jdbcTemplate.query(query, new BeanPropertyRowMapper<>(User.class));
			if(users.isEmpty()) {
				logger.info("Users not found.");
				throw new NoSuchElementException("Users not found.");
			}
		} catch(DataRetrievalFailureException exception) {
			logger.error("Unable to map. Users not found.");
		} catch(DataAccessException exception) {
			logger.error("Error from database: " + exception + "\n");
		}
		
		return users;
	}

	@Override
	public void save(final User user) {
		transactionTemplate.execute(new TransactionCallback<Integer>() {

				@Override
				public Integer doInTransaction(TransactionStatus status) {
					
					try {
						
						final String selectIfExistsQuery = "SELECT email FROM \"user\" WHERE email = ?;";
						
						final String insertUserQuery = 
								"INSERT INTO \"user\" \r\n"
								+ "(first_name, last_name, phone, address, email, password) \r\n"
								+ "VALUES (?, ?, ?, ?, ?, ?);";
						
						final String setUserRoleQuery = "INSERT INTO user_role (user_id, role_id, date_from) VALUES (?, ?, current_timestamp);";
						
						KeyHolder keyHolder = new GeneratedKeyHolder();
						
						String existingEmail = null;
						
						/*It is recommended to check for existing user before trying insertion in transaction.*/
						/*Even if insertion fails, ID sequence will still auto-increment*/
						try {
							existingEmail = jdbcTemplate.queryForObject(
									selectIfExistsQuery,
									new Object[] {user.getEmail()},
									new int[] {Types.VARCHAR},
									String.class
								);
						} catch(EmptyResultDataAccessException exception) {
							logger.info("Username not found. Registration can commit.", exception);
						}
						
						if(existingEmail != null)
							throw new DuplicateKeyException("Username with email: " + existingEmail + " already exists.");
						
						jdbcTemplate.update(new PreparedStatementCreator() {
					
							@Override
							public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
								
								PreparedStatement statement = connection.prepareStatement(
																	insertUserQuery, 
																	Statement.RETURN_GENERATED_KEYS
																);
								
								statement.setObject(1, user.getFirstName(), Types.VARCHAR);
								statement.setObject(2, user.getLastName(), Types.VARCHAR);
								statement.setObject(3, user.getPhone(), Types.VARCHAR);
								statement.setObject(4, user.getAddress(), Types.VARCHAR);
								statement.setObject(5, user.getEmail(), Types.VARCHAR);
								statement.setObject(6, user.getPassword(), Types.VARCHAR);
								
								return statement;
							}
							
						}, keyHolder);
						
						final Long generatedUserId = (Long) keyHolder.getKeys().get("id");
						
						jdbcTemplate.update(new PreparedStatementCreator() {
					
							@Override
							public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
								PreparedStatement statement = connection.prepareStatement(setUserRoleQuery);
								
								statement.setObject(1, generatedUserId, Types.BIGINT);
								statement.setObject(2, 1, Types.BIGINT);
								
								return statement;
							}
							
						});
						
						return null;
						
					} catch(DuplicateKeyException exception) {
						status.setRollbackOnly();
						logger.info("Error from database: ", exception);
						throw new IllegalArgumentException("Username already in use.", exception);
					} catch(Exception exception) {
						status.setRollbackOnly();
						logger.info("Error from database: ", exception);
						throw new IllegalArgumentException("Transaction failed.", exception);
					}
				}
			}
		);
	}

	@Override
	public User findByEmail(final String email) {
		final String query = 
				"SELECT \r\n"
				+ "id, \r\n"
				+ "first_name, \r\n"
				+ "last_name, \r\n"
				+ "phone, \r\n"
				+ "email \r\n"
				+ "FROM \"user\" WHERE email = ?;";
		
		User user = new User();
		try {
			user = jdbcTemplate.query(query, new Object[] {email}, new int[] {Types.VARCHAR}, new ResultSetExtractor<User>() {

				@Override
				public User extractData(ResultSet rs) throws SQLException, DataAccessException {
					User user = new User();
					
					while(rs.next()) {
						if(user.getId() == null) {
							user.setId(rs.getLong(1));
							user.setFirstName(rs.getString(2));
							user.setLastName(rs.getString(3));
							user.setPhone(rs.getString(4));
							user.setEmail(rs.getString(5));
						}
					}
					
					return user;
				}
				
			});
			
			if(user.getId() == null) {
				logger.info("Username not found.");
				throw new UsernameNotFoundException("Username not found.");
			}
		} catch(DataRetrievalFailureException exception) {
			logger.error("Unable to map.");
		} catch(DataAccessException exception) {
			logger.error("Error from database: " + exception + "\n");
		}
		
		return user;
	}

	@Override
	public List<User> findAllByRole(final String name) {
		final String query = 
				"SELECT\r\n"
				+ "u.id,\r\n"
				+ "u.first_name,\r\n"
				+ "u.last_name,\r\n"
				+ "u.phone,\r\n"
				+ "u.email\r\n"
				+ "FROM user_role ur \r\n"
				+ "INNER JOIN \"user\" u on u.id = ur.user_id AND ur.date_to is null \r\n"
				+ "INNER JOIN \"role\" r on r.id = ur.role_id WHERE r.name  = ?;";
		
		List<User> users = new ArrayList<>();
		try {
			users = jdbcTemplate.query(query, new Object[] {name}, new int[] {Types.VARCHAR}, new RowMapper<User>() {

				@Override
				public User mapRow(ResultSet rs, int rowNum) throws SQLException {
					User user = new User();
					
					user.setId(rs.getLong(1));
					user.setFirstName(rs.getString(2));
					user.setLastName(rs.getString(3));
					user.setPhone(rs.getString(4));
					user.setEmail(rs.getString(5));
					
					return user;
				}
				
			});
			
			if(users.isEmpty()) {
				logger.info("Users not found.");
				throw new NoSuchElementException("Users not found.");
			}
			
		} catch(DataRetrievalFailureException exception) {
			logger.error("Unable to map.");
		} catch(DataAccessException exception) {
			logger.error("Error from database: " + exception + "\n");
		}
		
		return users;
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void revokeRole(Long roleId) {
		// TODO Auto-generated method stub
		
	}
}
