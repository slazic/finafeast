package com.example.FinaFeast.dao;

import java.util.List;

import com.example.FinaFeast.model.Restaurant;

public interface RestaurantDao {
	Restaurant findById(final Long id);
    
    List<Restaurant> findAll();
    
    void save(final Restaurant t);
    
    void update(final Restaurant t);
    
    void delete(final Long id);
}
