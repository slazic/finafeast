package com.example.FinaFeast.dao;

import java.util.List;

import com.example.FinaFeast.model.User;

public interface UserDao {
	
	User findByUsername(final String email);
	
	User findByEmail(final String email);
	
	List<User> findAllByRole(final String name);
		
	List<User> findAll();
	
	void save (final User user);
	
	void delete (final Long id);
	
	void revokeRole (final Long roleId);
}
