package com.example.FinaFeast.dao;

import java.util.List;

import com.example.FinaFeast.model.Order;

public interface OrderDao {
	List<Order> findById(final Long id); //admin
    
    List<Order> findAll(); //admin
        
    void save(final Order order, final List<Long> selectedDishesIds);
    
    void update(final Order order);
    
    void updateStatus(final Long orderId);
    
    void setDeliverer(final Long orderId, final Long delivererId);
    
    void delete(final Long id);
    
    List<Order> findAllOrdersPlacedInRestaurant(final Long managerId);
    
    List<Order> findAllOrdersPlacedInRestaurantByStatus(final Long managerId, final String status);
    
    List<Order> findAllOrdersAvailableForDelivery();
    
    List<Order> findAllOrdersByDeliverer(final Long delivererId);
        
    List<Order> findAllOrdersByUser(final Long userId);
       
}
