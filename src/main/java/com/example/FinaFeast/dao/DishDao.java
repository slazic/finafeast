package com.example.FinaFeast.dao;

import java.util.List;

import com.example.FinaFeast.model.Dish;

public interface DishDao {
	Dish findById(final Long id);
    
    List<Dish> findAll();
    
    List<Dish> findAllByOrderId(final Long orderId);
    
    void save(final Dish dish);
    
    void update(final Dish dish);
    
    void delete(final Long id);
}
