package com.example.FinaFeast.model.request.order;

import java.util.List;

public class OrderRequestDto {
	private Long restaurantId;
	private List<Long> selectedDishesIds;
	
	public OrderRequestDto() {
		
	}

	public OrderRequestDto(Long restaurantId, List<Long> selectedDishesIds) {
		super();
		this.restaurantId = restaurantId;
		this.selectedDishesIds = selectedDishesIds;
	}

	public Long getRestaurantId() {
		return restaurantId;
	}

	public void setRestaurantId(Long restaurantId) {
		this.restaurantId = restaurantId;
	}

	public List<Long> getSelectedDishesIds() {
		return selectedDishesIds;
	}

	public void setSelectedDishesIds(List<Long> selectedDishesIds) {
		this.selectedDishesIds = selectedDishesIds;
	}
}
