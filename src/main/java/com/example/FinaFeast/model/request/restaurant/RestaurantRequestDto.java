package com.example.FinaFeast.model.request.restaurant;

public class RestaurantRequestDto {
	private Long id;
	private String name;
	private String phone;
	private String address;
	private Long managerId;
	
	public RestaurantRequestDto() {
		
	}
	
	public RestaurantRequestDto(Long id, String name, String phone, String address, Long managerId) {
		super();
		this.id = id;
		this.name = name;
		this.phone = phone;
		this.address = address;
		this.managerId = managerId;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Long getManagerId() {
		return managerId;
	}

	public void setManagerId(Long managerId) {
		this.managerId = managerId;
	}
	
}
