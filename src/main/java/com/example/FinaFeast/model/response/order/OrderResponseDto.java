package com.example.FinaFeast.model.response.order;

import java.sql.Timestamp;

import com.example.FinaFeast.model.response.restaurant.RestaurantResponseDto;
import com.example.FinaFeast.model.response.user.UserResponseDto;

public class OrderResponseDto {
	private Long id;
	private Timestamp dateFrom;
	private Timestamp dateTo;
	private UserResponseDto user;
	private UserResponseDto deliverer;
	private RestaurantResponseDto restaurant;
	private String status;
	public OrderResponseDto() {
		super();
	}
	public OrderResponseDto(Long id, Timestamp dateFrom, Timestamp dateTo, UserResponseDto user,
			UserResponseDto deliverer, RestaurantResponseDto restaurant, String status) {
		super();
		this.id = id;
		this.dateFrom = dateFrom;
		this.dateTo = dateTo;
		this.user = user;
		this.deliverer = deliverer;
		this.restaurant = restaurant;
		this.status = status;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Timestamp getDateFrom() {
		return dateFrom;
	}
	public void setDateFrom(Timestamp dateFrom) {
		this.dateFrom = dateFrom;
	}
	public Timestamp getDateTo() {
		return dateTo;
	}
	public void setDateTo(Timestamp dateTo) {
		this.dateTo = dateTo;
	}
	public UserResponseDto getUser() {
		return user;
	}
	public void setUser(UserResponseDto user) {
		this.user = user;
	}
	public UserResponseDto getDeliverer() {
		return deliverer;
	}
	public void setDeliverer(UserResponseDto deliverer) {
		this.deliverer = deliverer;
	}
	public RestaurantResponseDto getRestaurant() {
		return restaurant;
	}
	public void setRestaurant(RestaurantResponseDto restaurant) {
		this.restaurant = restaurant;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
}
