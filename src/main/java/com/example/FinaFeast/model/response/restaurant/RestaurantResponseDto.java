package com.example.FinaFeast.model.response.restaurant;

import com.example.FinaFeast.model.response.user.UserResponseDto;

public class RestaurantResponseDto {
	private Long id;
	private String name;
	private String phone;
	private String address;
	private UserResponseDto manager;
	
	public RestaurantResponseDto() {
		
	}

	public RestaurantResponseDto(Long id, String name, String phone, String address, UserResponseDto manager) {
		super();
		this.id = id;
		this.name = name;
		this.phone = phone;
		this.address = address;
		this.manager = manager;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public UserResponseDto getManager() {
		return manager;
	}

	public void setManager(UserResponseDto manager) {
		this.manager = manager;
	}
	
}
