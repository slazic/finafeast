package com.example.FinaFeast.model;

import java.sql.Timestamp;

public class Order {
	private Long id;
	private Timestamp dateFrom;
	private Timestamp dateTo;
	private User user;
	private User deliverer;
	private Restaurant restaurant;
	private String status;
	
	public Order() {
		
	}

	public Order(Long id, Timestamp dateFrom, Timestamp dateTo, User user, User deliverer, Restaurant restaurant, String status) {
		super();
		this.id = id;
		this.dateFrom = dateFrom;
		this.dateTo = dateTo;
		this.user = user;
		this.deliverer = deliverer;
		this.restaurant = restaurant;
		this.status = status;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Timestamp getDateFrom() {
		return dateFrom;
	}

	public void setDateFrom(Timestamp dateFrom) {
		this.dateFrom = dateFrom;
	}

	public Timestamp getDateTo() {
		return dateTo;
	}

	public void setDateTo(Timestamp dateTo) {
		this.dateTo = dateTo;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public User getDeliverer() {
		return deliverer;
	}

	public void setDeliverer(User deliverer) {
		this.deliverer = deliverer;
	}

	public Restaurant getRestaurant() {
		return restaurant;
	}

	public void setRestaurant(Restaurant restaurant) {
		this.restaurant = restaurant;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
