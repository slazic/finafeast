package com.example.FinaFeast.model.mapper.implementation;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import com.example.FinaFeast.model.User;
import com.example.FinaFeast.model.mapper.EntityDtoMapper;
import com.example.FinaFeast.model.response.user.UserResponseDto;

@Component
public class UserToUserDtoMapper implements EntityDtoMapper<User, UserResponseDto>{

	@Override
	public UserResponseDto map(final User user) {
		UserResponseDto dto = new UserResponseDto();
		
		dto.setId(user.getId());
		dto.setFirstName(user.getFirstName());
		dto.setLastName(user.getLastName());
		dto.setPhone(user.getPhone());
		dto.setEmail(user.getEmail());
		
		return dto;
	}

	@Override
	public List<UserResponseDto> mapToList(final List<User> users) {
		return users.stream().map(this::map).collect(Collectors.toList());
	}

}
