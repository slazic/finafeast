package com.example.FinaFeast.model.mapper.implementation.restaurant;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import com.example.FinaFeast.model.Restaurant;
import com.example.FinaFeast.model.User;
import com.example.FinaFeast.model.mapper.EntityDtoMapper;
import com.example.FinaFeast.model.request.restaurant.RestaurantRequestDto;

@Component
public class RestaurantDtoToRestaurantMapper implements EntityDtoMapper<RestaurantRequestDto, Restaurant>{
	
	@Override
	public Restaurant map(final RestaurantRequestDto dto) {
		Restaurant restaurant = new Restaurant();
		User user = new User();
		
		user.setId(dto.getManagerId());
		
		restaurant.setId(dto.getId());
		restaurant.setName(dto.getName());
		restaurant.setPhone(dto.getPhone());
		restaurant.setAddress(dto.getAddress());
		restaurant.setManager(user);
		
		return restaurant;
	}

	@Override
	public List<Restaurant> mapToList(final List<RestaurantRequestDto> dtos) {
		return dtos.stream().map(this::map).collect(Collectors.toList());
	}

}
