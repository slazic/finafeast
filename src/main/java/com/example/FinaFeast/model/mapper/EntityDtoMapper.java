package com.example.FinaFeast.model.mapper;

import java.util.List;

public interface EntityDtoMapper<T, U> {
	U map (final T t);
	List<U> mapToList (final List<T> t);
}
