package com.example.FinaFeast.model.mapper.implementation.restaurant;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import com.example.FinaFeast.model.Restaurant;
import com.example.FinaFeast.model.mapper.EntityDtoMapper;
import com.example.FinaFeast.model.response.restaurant.RestaurantResponseDto;
import com.example.FinaFeast.model.response.user.UserResponseDto;

@Component
public class RestaurantToRestaurantDtoMapper implements EntityDtoMapper<Restaurant, RestaurantResponseDto>{

	@Override
	public RestaurantResponseDto map(final Restaurant restaurant) {
		RestaurantResponseDto dto = new RestaurantResponseDto();
		UserResponseDto userDto = new UserResponseDto();
		
		dto.setId(restaurant.getId());
		dto.setName(restaurant.getName());
		dto.setPhone(restaurant.getPhone());
		dto.setAddress(restaurant.getAddress());
		
		
		userDto.setId(restaurant.getManager().getId());
		userDto.setFirstName(restaurant.getManager().getFirstName());
		userDto.setLastName(restaurant.getManager().getLastName());
		userDto.setPhone(restaurant.getManager().getPhone());
		userDto.setEmail(restaurant.getManager().getEmail());
		
		dto.setManager(userDto);
		
		return dto;
	}

	@Override
	public List<RestaurantResponseDto> mapToList(final List<Restaurant> restaurant) {
		return restaurant.stream().map(this::map).collect(Collectors.toList());
	}

}
