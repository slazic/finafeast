package com.example.FinaFeast.model.mapper.implementation.order;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import com.example.FinaFeast.model.Order;
import com.example.FinaFeast.model.mapper.EntityDtoMapper;
import com.example.FinaFeast.model.response.order.OrderResponseDto;
import com.example.FinaFeast.model.response.restaurant.RestaurantResponseDto;
import com.example.FinaFeast.model.response.user.UserResponseDto;

@Component
public class OrderToOrderDtoMapper implements EntityDtoMapper<Order, OrderResponseDto>{

	@Override
	public OrderResponseDto map(final Order order) {
		OrderResponseDto orderDto = new OrderResponseDto();
		UserResponseDto user = new UserResponseDto();
		UserResponseDto deliverer = new UserResponseDto();
		UserResponseDto manager = new UserResponseDto();
		RestaurantResponseDto restaurant = new RestaurantResponseDto();
		
		orderDto.setId(order.getId());
		orderDto.setDateFrom(order.getDateFrom());
		orderDto.setDateTo(order.getDateTo());
		
		user.setId(order.getUser().getId());
		user.setFirstName(order.getUser().getFirstName());
		user.setLastName(order.getUser().getLastName());
		user.setPhone(order.getUser().getPhone());
		user.setAddress(order.getUser().getAddress());
		user.setEmail(order.getUser().getEmail());
		
		if(order.getDeliverer().getId() != null) {
			deliverer.setId(order.getDeliverer().getId());
			deliverer.setFirstName(order.getDeliverer().getFirstName());
			deliverer.setLastName(order.getDeliverer().getLastName());
			deliverer.setPhone(order.getDeliverer().getPhone());
			deliverer.setAddress(order.getDeliverer().getAddress());
			deliverer.setEmail(order.getDeliverer().getEmail());
		}
		
		manager.setId(order.getRestaurant().getManager().getId());
		manager.setFirstName(order.getRestaurant().getManager().getFirstName());
		manager.setLastName(order.getRestaurant().getManager().getLastName());
		manager.setPhone(order.getRestaurant().getManager().getPhone());
		manager.setAddress(order.getRestaurant().getManager().getAddress());
		manager.setEmail(order.getRestaurant().getManager().getEmail());
		
		restaurant.setId(order.getRestaurant().getId());
		restaurant.setName(order.getRestaurant().getName());
		restaurant.setPhone(order.getRestaurant().getPhone());
		restaurant.setAddress(order.getRestaurant().getAddress());

		
		restaurant.setManager(manager);
		
		orderDto.setUser(user);
		
		if(deliverer.getId() != null) {
			orderDto.setDeliverer(deliverer);
		}
		
		orderDto.setRestaurant(restaurant);
		orderDto.setStatus(order.getStatus());
		

		return orderDto;
	}

	@Override
	public List<OrderResponseDto> mapToList(final List<Order> orders) {
		return orders.stream().map(this::map).collect(Collectors.toList());
	}

}
