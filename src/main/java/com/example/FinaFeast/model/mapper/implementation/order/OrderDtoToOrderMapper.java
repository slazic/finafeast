package com.example.FinaFeast.model.mapper.implementation.order;

import java.util.List;

import org.springframework.stereotype.Component;

import com.example.FinaFeast.model.Order;
import com.example.FinaFeast.model.Restaurant;
import com.example.FinaFeast.model.mapper.EntityDtoMapper;
import com.example.FinaFeast.model.request.order.OrderRequestDto;

@Component
public class OrderDtoToOrderMapper implements EntityDtoMapper<OrderRequestDto, Order>{
	
	@Override
	public Order map(final OrderRequestDto dto) {
		Order order = new Order();
		Restaurant restaurant = new Restaurant();
		
		restaurant.setId(dto.getRestaurantId());
		
		order.setRestaurant(restaurant);
		
		return order;
	}

	@Override
	public List<Order> mapToList(List<OrderRequestDto> t) {
		// TODO Auto-generated method stub
		return null;
	}

}
