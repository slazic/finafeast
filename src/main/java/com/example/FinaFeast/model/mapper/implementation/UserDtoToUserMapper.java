package com.example.FinaFeast.model.mapper.implementation;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import com.example.FinaFeast.model.User;
import com.example.FinaFeast.model.mapper.EntityDtoMapper;
import com.example.FinaFeast.model.request.user.UserRegistrationRequestDto;

@Component
public class UserDtoToUserMapper implements EntityDtoMapper<UserRegistrationRequestDto, User>{
	
	private PasswordEncoder passwordEncoder;
	
	

	public UserDtoToUserMapper(
			final PasswordEncoder passwordEncoder
	) {
		this.passwordEncoder = passwordEncoder;
	}

	@Override
	public User map(final UserRegistrationRequestDto dto) {
		User user = new User();
		
		user.setFirstName(dto.getFirstName());
		user.setLastName(dto.getLastName());
		user.setPhone(dto.getPhone());
		user.setAddress(dto.getAddress());
		user.setEmail(dto.getEmail());
		user.setPassword(passwordEncoder.encode(dto.getPassword()));
		
		return user;
	}

	@Override
	public List<User> mapToList(final List<UserRegistrationRequestDto> dtos) {
		return dtos.stream().map(this::map).collect(Collectors.toList());
	}

}
