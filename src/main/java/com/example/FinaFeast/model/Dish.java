package com.example.FinaFeast.model;

public class Dish {
	private Long id;
	private String dishName;
	private Double price;
	private String description;
	
	public Dish() {
		
	}
	
	public Dish(Long id, String dishName, Double price, String description) {
		super();
		this.id = id;
		this.dishName = dishName;
		this.price = price;
		this.description = description;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getDishName() {
		return dishName;
	}
	public void setDishName(String dishName) {
		this.dishName = dishName;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
}
