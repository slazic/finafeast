package com.example.FinaFeast.configuration;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.crypto.SecretKey;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;

@Service
public class JwtService {
	
	@Value("${ff.jwt.secret}")
	private String SECRET;
	
	@Value("${ff.jwt.issuer}")
	private String ISSUER;
	
	@Value("${ff.jwt.duration}")
	private Long DURATION;
	
	
	private SecretKey getSigningKey() {
        byte[] keyBytes = Decoders.BASE64.decode(SECRET);
        return Keys.hmacShaKeyFor(keyBytes);
    }
	
	private Claims extractAllClaims(final String token) {
        return Jwts
        		.parser()
        		.verifyWith(getSigningKey())
        		.build()
        		.parseSignedClaims(token)
        		.getPayload();
    }
	
	private <T> T extractClaim(final String token, final Function<Claims, T> claimsResolvers) {
        final Claims claims = extractAllClaims(token);
        return claimsResolvers.apply(claims);
    }
	
	private Date extractExpiration(final String token) {
        return extractClaim(token, Claims::getExpiration);
    }
	
	private boolean isTokenExpired(final String token) {
        return extractExpiration(token).before(new Date());
    }
	
	private String generateToken(
			final Map<String, ?> extraClaims,
			final UserDetails userDetails
	) {
		return Jwts
				.builder()
				.subject(userDetails.getUsername())
				.issuer(ISSUER)
				.issuedAt(new Date(System.currentTimeMillis()))
				.expiration(new Date(System.currentTimeMillis() + DURATION))
				.claims().add(extraClaims).and()
				.signWith(getSigningKey())
				.compact();
	}
	
	public String extractUsername(final String token) {
        return extractClaim(token, Claims::getSubject);
    }
	
	public boolean isTokenValid(final String token, final UserDetails userDetails) {
        final String username = extractUsername(token);
        return (username.equals(userDetails.getUsername())) && !isTokenExpired(token);
    }
	
	public String generateToken(
			final UserDetails userDetails
	) {
		final List<String> userRoles = userDetails.getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList());
		
		final Map<String, Object> extraClaims = new HashMap<String, Object>();
		
		extraClaims.put("roles", userRoles);
		
		return generateToken(extraClaims, userDetails);
	}
}
