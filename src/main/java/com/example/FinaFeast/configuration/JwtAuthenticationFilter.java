package com.example.FinaFeast.configuration;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import com.example.FinaFeast.service.UserService;

@Component
public class JwtAuthenticationFilter extends OncePerRequestFilter{
	
	private JwtService jwtService;
	private UserService userService;
	
	
	public JwtAuthenticationFilter(
			JwtService jwtService, 
			UserService userService
	) {
		this.jwtService = jwtService;
		this.userService = userService;
	}

	@Override
	protected void doFilterInternal(
			HttpServletRequest request, 
			HttpServletResponse response, 
			FilterChain filterChain
	) throws ServletException, IOException {
		final String authenticationHeader = request.getHeader("Authorization");
		final String jsonWebToken;
		final String userEmail;
		
		if (!StringUtils.hasLength(authenticationHeader) || !StringUtils.startsWithIgnoreCase(authenticationHeader, "Bearer ")) {
			filterChain.doFilter(request, response);
			return;
		}
		
		jsonWebToken = authenticationHeader.substring(7);
		userEmail = jwtService.extractUsername(jsonWebToken);
		
		if(
			StringUtils.hasLength(userEmail)
			&& SecurityContextHolder.getContext().getAuthentication() == null
		) {
			UserDetails userDetails = userService.loadUserByUsername(userEmail);
			
			if(jwtService.isTokenValid(jsonWebToken, userDetails)) {
				UsernamePasswordAuthenticationToken authenticationToken =
						new UsernamePasswordAuthenticationToken(
							userDetails,
							null,
							userDetails.getAuthorities()
						);
				
				authenticationToken.setDetails(
						new WebAuthenticationDetailsSource().buildDetails(request)
				);
				
				SecurityContextHolder.getContext().setAuthentication(authenticationToken);
			}
		}
		
		filterChain.doFilter(request, response);
	}

}
