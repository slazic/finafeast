package com.example.FinaFeast.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration {
	
	private JwtAuthenticationFilter jwtAuthenticationFilter;
	
	private AuthenticationProvider authenticationProvider;
	
	public SecurityConfiguration(
			JwtAuthenticationFilter jwtAuthenticationFilter,
			AuthenticationProvider authenticationProvider
	) {
		this.jwtAuthenticationFilter = jwtAuthenticationFilter;
		this.authenticationProvider = authenticationProvider;
	}
	
	

	@Bean
    SecurityFilterChain securityFilterChain(
    		final HttpSecurity http
    ) throws Exception {

        http
                .csrf(AbstractHttpConfigurer::disable)
                .cors(Customizer.withDefaults())
                .logout((logout) -> {
                	logout.logoutSuccessHandler((httpServerletRequest, httpServerletResponse, authentication) -> {
                		httpServerletResponse.setStatus(200);
                	});
                })
                .sessionManagement((management) -> {
                    management.sessionCreationPolicy(SessionCreationPolicy.STATELESS);
                })
                .authenticationProvider(authenticationProvider)
                .addFilterBefore(
                        jwtAuthenticationFilter,
                        UsernamePasswordAuthenticationFilter.class
                )
                .authorizeHttpRequests((requests) -> {
                    requests
                            .antMatchers(
                                    "/"
                            ).permitAll()

                            /*DISH*/
                            .antMatchers(HttpMethod.POST, "/dishes/**").hasAnyRole("ADMIN", "MANAGER")
                            .antMatchers(HttpMethod.PUT, "/dishes/**").hasRole("ADMIN")
                            .antMatchers(HttpMethod.DELETE, "/dishes/**").hasRole("ADMIN")
                            .antMatchers(HttpMethod.GET, "/dishes/**").permitAll()

                            /*USER*/
                            .antMatchers("/users/all").hasRole("ADMIN")
                            .antMatchers("users/allByRole").hasRole("ADMIN")
                            .antMatchers(HttpMethod.GET, "/users/**").permitAll()
                            .antMatchers(HttpMethod.POST, "/users/registration").permitAll()
                            .antMatchers(HttpMethod.POST, "/users/login").permitAll()

                            /*RESTAURANT*/
                            .antMatchers(HttpMethod.POST, "/restaurants/**").hasAnyRole("ADMIN", "MANAGER")
                            .antMatchers(HttpMethod.PUT, "/restaurants/**").hasRole("ADMIN")
                            .antMatchers(HttpMethod.DELETE, "/restaurants/**").hasRole("ADMIN")
                            .antMatchers(HttpMethod.GET, "/restaurants/**").permitAll()

                            /*ORDER*/
                            .antMatchers(HttpMethod.GET, "/orders/all").hasRole("ADMIN")
                            .antMatchers(HttpMethod.GET, "/orders/restaurant/**").hasAnyRole("ADMIN", "MANAGER")
                            .antMatchers(HttpMethod.GET, "/orders/deliverer/**").hasAnyRole("ADMN", "DELIVERER")
                            .antMatchers(HttpMethod.GET, "/orders/user/**").hasAnyRole("ADMN", "USER")
                            .antMatchers(HttpMethod.GET, "/orders/**").hasAnyRole("USER", "ADMIN", "DELIVERER", "MANAGER")
                            .antMatchers(HttpMethod.POST, "/orders/**").hasAnyRole("USER", "ADMIN")
                            .antMatchers(HttpMethod.PUT, "/orders/setDeliverer").hasAnyRole("ADMIN", "DELIVERER")
                            .antMatchers(HttpMethod.PUT, "/orders/**").hasAnyRole("ADMIN", "MANAGER", "DELIVERER")
                            .antMatchers(HttpMethod.DELETE, "/orders/**").hasRole("ADMIN")
                            .anyRequest().authenticated();

                });  
    	
        return http.build();
	 }

}
