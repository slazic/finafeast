package com.example.FinaFeast.controller;

import java.util.List;
import java.util.NoSuchElementException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.FinaFeast.model.User;
import com.example.FinaFeast.model.mapper.EntityDtoMapper;
import com.example.FinaFeast.model.request.user.UserLoginRequestDto;
import com.example.FinaFeast.model.request.user.UserRegistrationRequestDto;
import com.example.FinaFeast.model.response.user.UserResponseDto;
import com.example.FinaFeast.service.AuthenticationService;
import com.example.FinaFeast.service.UserService;

@RestController
@RequestMapping("/users")
public class UserController {
	
	private UserService userService;
	
	private AuthenticationService authenticationService;
	
	private EntityDtoMapper<User, UserResponseDto> userToDtoMapper;
	
	private EntityDtoMapper<UserRegistrationRequestDto, User> dtoToUserMapper;
	
	public UserController(
			final UserService userService,
			final AuthenticationService authenticationService,
			final EntityDtoMapper<User, UserResponseDto> userToDtoMapper,
			final EntityDtoMapper<UserRegistrationRequestDto, User> dtoToUserMapper
	) {
		this.userService = userService;
		this.authenticationService = authenticationService;
		this.userToDtoMapper = userToDtoMapper;
		this.dtoToUserMapper = dtoToUserMapper;
	}
	
	private static Logger logger = LoggerFactory.getLogger(UserController.class);

	
	@GetMapping("/all")
	public ResponseEntity<?> getAllUsers() {
		try {
			List<UserResponseDto> dtos = userToDtoMapper.mapToList(userService.loadAll());
			return ResponseEntity.status(HttpStatus.OK).body(dtos);
		} 
		catch(NoSuchElementException exception) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(exception.getMessage());
		} 
		catch (Exception exception) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(exception.getMessage());
		}
	}
	
	@GetMapping("/allByRole")
	public ResponseEntity<?> getAllUsersByRole(@RequestParam final String name) {
		try {
			List<UserResponseDto> dtos = userToDtoMapper.mapToList(userService.loadAllByRole(name));
			return ResponseEntity.status(HttpStatus.OK).body(dtos);
		} 
		catch(NoSuchElementException exception) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(exception.getMessage());
		} 
		catch (Exception exception) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(exception.getMessage());
		}
	}
	
	@GetMapping("/user")
	public ResponseEntity<?> getUserByUsername(@RequestParam final String email) {
		try {
			UserResponseDto dto = userToDtoMapper.map(userService.loadByEmail(email));
			return ResponseEntity.status(HttpStatus.OK).body(dto);
		} catch (Exception exception) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(exception.getMessage());
		}
	}
	
	@PostMapping("/registration")
	public ResponseEntity<?> registerUser(@RequestBody final UserRegistrationRequestDto dto) {
		try {
			authenticationService.register(dtoToUserMapper.map(dto));
			return ResponseEntity.status(HttpStatus.CREATED).body("Sucessfully registered.");
		} catch(Exception exception) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(exception.getMessage());
		}
	}
	
	@PostMapping("/login")
	public ResponseEntity<?> loginUser(@RequestBody final UserLoginRequestDto dto) {
		try {
			return ResponseEntity.status(HttpStatus.OK).body(authenticationService.login(dto));
		} catch(Exception exception) {
			logger.error("Error: " + exception + "\n");
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(exception.getMessage());
		}
	}
}
