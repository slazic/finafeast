package com.example.FinaFeast.controller;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.FinaFeast.model.Dish;
import com.example.FinaFeast.service.DishService;

@RestController
@RequestMapping("/dishes")
public class DishController {
	
	private DishService dishService;
	
	public DishController(
		final DishService dishService
	) {
		this.dishService = dishService;
	}
	
	@GetMapping("/all")
	public ResponseEntity<?> getAllDishes() {
		try {
			List<Dish> dishes = dishService.loadAll();
			return ResponseEntity.status(HttpStatus.OK).body(dishes);
		}
		catch (NoSuchElementException exception) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(exception.getMessage());
		} 
		catch (Exception exception) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(exception.getMessage());
		}
	}
	
	@GetMapping("/allBy")
	public ResponseEntity<?> getAllDishesByOrderId(
			@RequestParam final Long orderId
	) {
		try {
			List<Dish> dishes = dishService.loadAllByOrderId(orderId);
			return ResponseEntity.status(HttpStatus.OK).body(dishes);
		} catch (Exception exception) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(exception.getMessage());
		}
	}
	
	@GetMapping("/dish")
	public ResponseEntity<?> getDishById(@RequestParam final Long id) {
		try {
			Dish dish = dishService.loadById(id);
			return ResponseEntity.status(HttpStatus.OK).body(dish);
		} catch (Exception exception) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(exception.getMessage());
		}
	}
	
	@PostMapping("/create")
	public ResponseEntity<?> createDish(@RequestBody final Dish dish) {
		try {
			dishService.create(dish);
			return ResponseEntity.status(HttpStatus.CREATED).body("Dish created sucessfully.");
		} catch (Exception exception) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(exception.getMessage());
		}
	}
	
	@DeleteMapping("/delete")
	public ResponseEntity<?> deleteDish(@RequestParam final Long id) {
		try {
			dishService.delete(id);
			return ResponseEntity.status(HttpStatus.OK).body("Dish delete sucessfully.");
		} catch (Exception exception) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(exception.getMessage());
		}
	}
	
	@PutMapping("/update")
	public ResponseEntity<?> updateDish(
			@RequestBody final Dish dish
	) {
		try {
			dishService.update(dish);
			return ResponseEntity.status(HttpStatus.OK).body("Dish updated sucessfully.");
		} catch(Exception exception) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(exception.getMessage());
		}
	}
}
