package com.example.FinaFeast.controller;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.FinaFeast.model.Restaurant;
import com.example.FinaFeast.model.mapper.EntityDtoMapper;
import com.example.FinaFeast.model.request.restaurant.RestaurantRequestDto;
import com.example.FinaFeast.model.response.restaurant.RestaurantResponseDto;
import com.example.FinaFeast.service.RestaurantService;

@RestController
@RequestMapping("/restaurants")
public class RestaurantController {
	private RestaurantService restaurantService;
	
	private EntityDtoMapper<Restaurant, RestaurantResponseDto> entityToDtoMapper;
	private EntityDtoMapper<RestaurantRequestDto, Restaurant> dtoToEntityMapper;


	public RestaurantController(
			final RestaurantService restaurantService,
			final EntityDtoMapper<Restaurant, RestaurantResponseDto> entityToDtoMapper,
			final EntityDtoMapper<RestaurantRequestDto, Restaurant> dtoToEntityMapper
	) {
		this.restaurantService = restaurantService;
		this.entityToDtoMapper = entityToDtoMapper;
		this.dtoToEntityMapper = dtoToEntityMapper;
	}
	
	@GetMapping("/restaurant")
	public ResponseEntity<?> getRestaurantById(@RequestParam final Long id) {
		try {
			RestaurantResponseDto dto = entityToDtoMapper.map(restaurantService.loadById(id));
			return ResponseEntity.status(HttpStatus.OK).body(dto);
		} catch (Exception exception) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(exception.getMessage());
		}
	}
	
	@GetMapping("/all")
	public ResponseEntity<?> getAllRestaurants() {
		try {
			List<RestaurantResponseDto> dtos = entityToDtoMapper.mapToList(restaurantService.loadAll());
			return ResponseEntity.status(HttpStatus.OK).body(dtos);
		} 
		catch(NoSuchElementException exception) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(exception.getMessage());
		} 
		catch (Exception exception) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(exception.getMessage());
		}
	}
	
	 @PostMapping("/create")
	 public ResponseEntity<?> crateRestaurant(
			 @RequestBody RestaurantRequestDto dto
	 ) {
		try {
			restaurantService.create(dtoToEntityMapper.map(dto));
			return ResponseEntity.status(HttpStatus.OK).body("Restaurant created."); 
		} catch (Exception exception) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(exception.getMessage());
		} 
	 }
	 
	 @DeleteMapping("/delete")
	 public ResponseEntity<?> deleteRestaurant(@RequestParam final Long id) {
		 try {
			 restaurantService.delete(id);
			 return ResponseEntity.status(HttpStatus.OK).body("Restaurant deleted sucessfully.");
		 } catch (Exception exception) {
			 return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(exception.getMessage());
		 }
	 }
	 
	 @PutMapping("/update")
	 public ResponseEntity<?> updateRestaurant(
		@RequestBody final RestaurantRequestDto restaurant
	 ) {
		 try {
			 restaurantService.update(dtoToEntityMapper.map(restaurant));
			 return ResponseEntity.status(HttpStatus.OK).body("Restaurant updated sucessfully.");
		 } catch(Exception exception) {
			 return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(exception.getMessage());
		 }
	 }
	 
}
