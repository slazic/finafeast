package com.example.FinaFeast.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.FinaFeast.model.Order;
import com.example.FinaFeast.model.mapper.EntityDtoMapper;
import com.example.FinaFeast.model.request.order.OrderRequestDto;
import com.example.FinaFeast.model.response.order.OrderResponseDto;
import com.example.FinaFeast.service.OrderService;


@RestController
@RequestMapping("/orders")
public class OrderController {
	
	private OrderService orderService;
	
	private EntityDtoMapper<OrderRequestDto, Order> dtoToEntityMapper;
	
	private EntityDtoMapper<Order, OrderResponseDto> entityToDtoMapper;
	
	public OrderController(
			OrderService orderService,
			EntityDtoMapper<OrderRequestDto, Order> dtoToEntityMapper,
			EntityDtoMapper<Order, OrderResponseDto> entityToDtoMapper
	) {
		this.orderService = orderService;
		this.dtoToEntityMapper = dtoToEntityMapper;
		this.entityToDtoMapper = entityToDtoMapper;
	}



	@GetMapping("/all")
	public ResponseEntity<?> getAllOrdersByCreatedStatus() {
		try {
			List<OrderResponseDto> dtos = entityToDtoMapper.mapToList(orderService.loadAll());
			return ResponseEntity.status(HttpStatus.OK).body(dtos);
		} catch (Exception exception) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(exception.getMessage());
		}
	}
	
	/* @GetMapping("/order")
	public ResponseEntity<?> getRestaurantById(@RequestParam final Long id) {
		try {
			List<AdminOrderResponseDto> dtos = entityToAdminDtoMapper.mapToList(orderService.loadById(id));
			return ResponseEntity.status(HttpStatus.OK).body(dtos);
		} catch (Exception exception) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(exception.getMessage());
		}
	} */
	
	@PostMapping("/create")
	public ResponseEntity<?> createOrder(
			@RequestBody final OrderRequestDto dto,
			HttpServletRequest request
	) {
		try {
			final String principalEmail = request.getUserPrincipal().getName();
			orderService.create(principalEmail, dtoToEntityMapper.map(dto), dto.getSelectedDishesIds());
			return ResponseEntity.status(HttpStatus.CREATED).body("Order created sucessfully.");
		} catch (Exception exception) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(exception.getMessage());
		}
	}
	
	@DeleteMapping("/delete")
	 public ResponseEntity<?> deleteOrder(@RequestParam final Long id) {
		 try {
			 orderService.delete(id);
			 return ResponseEntity.status(HttpStatus.OK).body("Order deleted sucessfully.");
		 } catch (Exception exception) {
			 return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(exception.getMessage());
		 }
	 }
	
	@PutMapping("/updateStatus")
	 public ResponseEntity<?> updateOrderStatus(
		@RequestParam final Long orderId
	 ) {
		 try {
			 orderService.updateStatus(orderId);
			 return ResponseEntity.status(HttpStatus.OK).body("Order status updated sucessfully.");
		 } catch(Exception exception) {
			 return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(exception.getMessage());
		 }
	 }
	
	@PutMapping("/setDeliverer")
	 public ResponseEntity<?> setOrderdeliverer(
		@RequestParam final Long orderId,
		HttpServletRequest request
	 ) {
		 try {
			 final String principalEmail = request.getUserPrincipal().getName();
			 orderService.setDeliverer(orderId, principalEmail);
			 return ResponseEntity.status(HttpStatus.OK).body("Deliverer set sucessfully.");
		 } catch(Exception exception) {
			 return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(exception.getMessage());
		 }
	 }
	
	@GetMapping("/restaurant/all")
	public ResponseEntity<?> getAllOrdersPlacedInRestaurant(
			HttpServletRequest request
	) {
		try {
			final String managerEmail = request.getUserPrincipal().getName();
			List<OrderResponseDto> dtos = entityToDtoMapper.mapToList(orderService.loadAllOrdersPlacedInRestaurant(managerEmail));
			return ResponseEntity.status(HttpStatus.OK).body(dtos);
		} catch (Exception exception) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(exception.getMessage());
		}
	}
	
	/* @GetMapping("/restaurant/allBy")
	public ResponseEntity<?> getAllOrdersPlacedInRestaurantByStatus(
			@RequestParam final String status,
			HttpServletRequest request
	) {
		try {
			final String managerEmail = request.getUserPrincipal().getName();
			List<RestaurantOrderResponseDto> dtos = entityToRestaurantDtoMapper.mapToList(orderService.loadAllOrdersPlacedInRestaurantByStatus(managerEmail, status));
			return ResponseEntity.status(HttpStatus.OK).body(dtos);
		} catch (Exception exception) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(exception.getMessage());
		}
	} */
	
	@GetMapping("/deliverer/available")
	public ResponseEntity<?> getAllOrdersAvailableForDelivery() {
		try {
			List<OrderResponseDto> dtos = entityToDtoMapper.mapToList(orderService.loadAllOrdersAvailableForDelivery());
			return ResponseEntity.status(HttpStatus.OK).body(dtos);
		} catch (Exception exception) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(exception.getMessage());
		}
	}
	
	@GetMapping("/deliverer/all")
	public ResponseEntity<?> getAllOrdersByDeliverer(
			HttpServletRequest request
	) {
		try {
			final String delivererEmail = request.getUserPrincipal().getName();
			List<OrderResponseDto> dtos = entityToDtoMapper.mapToList(orderService.findAllOrdersByDeliverer(delivererEmail));
			return ResponseEntity.status(HttpStatus.OK).body(dtos);
		} catch (Exception exception) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(exception.getMessage());
		}
	}
	
	@GetMapping("/user/all")
	public ResponseEntity<?> getAllOrdersByUser(
			HttpServletRequest request
	) {
		try {
			final String userEmail = request.getUserPrincipal().getName();
			List<OrderResponseDto> dtos = entityToDtoMapper.mapToList(orderService.loadAllOrdersByUser(userEmail));
			return ResponseEntity.status(HttpStatus.OK).body(dtos);
		} catch (Exception exception) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(exception.getMessage());
		}
	}
}
