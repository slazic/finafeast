package com.example.FinaFeast;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FinaFeastApplication {

	public static void main(String[] args) {
		SpringApplication.run(FinaFeastApplication.class, args);
	}

}
