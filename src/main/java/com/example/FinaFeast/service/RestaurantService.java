package com.example.FinaFeast.service;

import java.util.List;

import com.example.FinaFeast.model.Restaurant;

public interface RestaurantService {
	Restaurant loadById(final Long id);
	
	List<Restaurant> loadAll();
	
	void create(final Restaurant t);
	
	void update(final Restaurant t);
	
	void delete(final Long id);
}
