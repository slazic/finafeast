package com.example.FinaFeast.service.implementation;

import java.util.List;

import org.springframework.stereotype.Service;

import com.example.FinaFeast.dao.OrderDao;
import com.example.FinaFeast.model.Order;
import com.example.FinaFeast.model.User;
import com.example.FinaFeast.service.OrderService;
import com.example.FinaFeast.service.UserService;

@Service
public class OrderServiceImpl implements OrderService{
	
	private OrderDao orderDao;
	
	private UserService userService;

	public OrderServiceImpl(
		final OrderDao orderDao,
		final UserService userService
	) {
		this.orderDao = orderDao;
		this.userService = userService;
	}
	
	@Override
	public List<Order> loadById(Long id) {
		return orderDao.findById(id);
	}

	@Override
	public List<Order> loadAll() {
		return orderDao.findAll();
	}

	@Override
	public void create(final String userEmail, final Order order, final List<Long> selectedDishesIds) {
		User user = userService.loadByEmail(userEmail);
		order.setUser(user);
		orderDao.save(order, selectedDishesIds);
	}

	@Override
	public void update(Order t) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(final Long id) {
		orderDao.delete(id);
		
	}

	@Override
	public void updateStatus(final Long orderId) {
		orderDao.updateStatus(orderId);
	}

	@Override
	public void setDeliverer(final Long orderId, final String principalEmail) {
		User user = userService.loadByEmail(principalEmail);
		orderDao.setDeliverer(orderId, user.getId());
	}

	@Override
	public List<Order> loadAllOrdersPlacedInRestaurant(final String managerEmail) {
		User user = userService.loadByEmail(managerEmail);
		return orderDao.findAllOrdersPlacedInRestaurant(user.getId());
	}

	@Override
	public List<Order> loadAllOrdersPlacedInRestaurantByStatus(String managerEmail, String status) {
		User user = userService.loadByEmail(managerEmail);
		return orderDao.findAllOrdersPlacedInRestaurantByStatus(user.getId(), status);
	}

	@Override
	public List<Order> loadAllOrdersAvailableForDelivery() {
		return orderDao.findAllOrdersAvailableForDelivery();
	}

	@Override
	public List<Order> findAllOrdersByDeliverer(String delivererEmail) {
		User user = userService.loadByEmail(delivererEmail);
		return orderDao.findAllOrdersByDeliverer(user.getId());
	}

	@Override
	public List<Order> loadAllOrdersByUser(final String userEmail) {
		User user = userService.loadByEmail(userEmail);
		return orderDao.findAllOrdersByUser(user.getId());
	}

}
