package com.example.FinaFeast.service.implementation;

import java.util.List;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import com.example.FinaFeast.dao.UserDao;
import com.example.FinaFeast.model.User;
import com.example.FinaFeast.service.UserService;

@Service
public class UserServiceImpl implements UserService{
	
	private UserDao userDao;
		
	public UserServiceImpl(UserDao userDao) {
		this.userDao = userDao;
	}
	
	//related to SpringSecurity
	@Override
	public UserDetails loadUserByUsername(final String email) {
		return userDao.findByUsername(email);
	}

	@Override
	public List<User> loadAll() {
		return userDao.findAll();
	}

	@Override
	public User loadByEmail(final String email) {
		return userDao.findByEmail(email);
	}

	@Override
	public List<User> loadAllByRole(final String name) {
		return userDao.findAllByRole(name);
	}
}
