package com.example.FinaFeast.service.implementation;

import java.util.List;

import org.springframework.stereotype.Service;

import com.example.FinaFeast.dao.RestaurantDao;
import com.example.FinaFeast.model.Restaurant;
import com.example.FinaFeast.service.RestaurantService;

@Service
public class RestaurantServiceImpl implements RestaurantService{
	
	private RestaurantDao restaurantDao;

	public RestaurantServiceImpl(
		final RestaurantDao restaurantDao
	) {
		this.restaurantDao = restaurantDao;
	}
	
	@Override
	public Restaurant loadById(final Long id) {
		return restaurantDao.findById(id);
	}

	@Override
	public List<Restaurant> loadAll() {
		return restaurantDao.findAll();
	}

	@Override
	public void create(final Restaurant restaurant) {
		restaurantDao.save(restaurant);
	}

	@Override
	public void update(final Restaurant restaurant) {
		restaurantDao.update(restaurant);
	}

	@Override
	public void delete(Long id) {
		restaurantDao.delete(id);
	}

}
