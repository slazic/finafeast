package com.example.FinaFeast.service.implementation;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Service;

import com.example.FinaFeast.configuration.JwtService;
import com.example.FinaFeast.dao.UserDao;
import com.example.FinaFeast.model.User;
import com.example.FinaFeast.model.request.user.UserLoginRequestDto;
import com.example.FinaFeast.service.AuthenticationService;

@Service
public class AuthenticationServiceImpl implements AuthenticationService{
	
	private UserDao userDao;
	
	private JwtService jwtService;
	
	private AuthenticationManager authenticationManager;
	
	public AuthenticationServiceImpl(
			UserDao userDao, 
			JwtService jwtService,
			AuthenticationManager authenticationManager
	) {
		this.userDao = userDao;
		this.jwtService = jwtService;
		this.authenticationManager = authenticationManager;
	}

	@Override
	public String login(final UserLoginRequestDto dto) {
		User user = userDao.findByUsername(dto.getEmail());
		
		authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(
						dto.getEmail(),
						dto.getPassword()
				)
		);
		
		return jwtService.generateToken(user);
	}

	@Override
	public void register(final User user) {
		userDao.save(user);
	}

}
