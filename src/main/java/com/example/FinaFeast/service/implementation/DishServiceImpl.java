package com.example.FinaFeast.service.implementation;

import java.util.List;

import org.springframework.stereotype.Service;

import com.example.FinaFeast.dao.DishDao;
import com.example.FinaFeast.model.Dish;
import com.example.FinaFeast.service.DishService;

@Service
public class DishServiceImpl implements DishService{
	
	private DishDao dishDao;

	public DishServiceImpl(
		final DishDao dishDao
	) {
		this.dishDao = dishDao;
	}

	@Override
	public Dish loadById(final Long id) {
		return dishDao.findById(id);
	}
	
	@Override
	public List<Dish> loadAll() {
		return dishDao.findAll();
	}

	@Override
	public void create(final Dish dish) {
		dishDao.save(dish);
	}

	@Override
	public void delete(final Long id) {
		dishDao.delete(id);
	}

	@Override
	public void update(final Dish dish) {
		dishDao.update(dish);
	}

	@Override
	public List<Dish> loadAllByOrderId(Long orderId) {
		return dishDao.findAllByOrderId(orderId);
	}
	
}
