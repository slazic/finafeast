package com.example.FinaFeast.service;

import java.util.List;

import com.example.FinaFeast.model.Order;

public interface OrderService {
	List<Order> loadById(final Long id);
	
	List<Order> loadAll();
	
	//List<Order> loadAllbyOrderStatus(final String status);
	
	void create(final String userEmail, final Order order, final List<Long> selectedDishesIds);
	
	void update(final Order order);
	
	void updateStatus(final Long orderId);
	
	void setDeliverer(final Long orderId, final String principalEmail);
	
	void delete(final Long id);
	
    List<Order> loadAllOrdersPlacedInRestaurant(final String managerEmail);
    
    List<Order> loadAllOrdersPlacedInRestaurantByStatus(final String managerEmail, final String status);
    
    List<Order> loadAllOrdersAvailableForDelivery();
    
    List<Order> findAllOrdersByDeliverer(final String delivererEmail);
    
    List<Order> loadAllOrdersByUser(final String userEmail);
    
}
