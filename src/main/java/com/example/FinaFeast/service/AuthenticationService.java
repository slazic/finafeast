package com.example.FinaFeast.service;

import com.example.FinaFeast.model.User;
import com.example.FinaFeast.model.request.user.UserLoginRequestDto;

public interface AuthenticationService {
	String login(final UserLoginRequestDto dto);

	void register(final User user);
}
