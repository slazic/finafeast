package com.example.FinaFeast.service;

import java.util.List;

import org.springframework.security.core.userdetails.UserDetailsService;

import com.example.FinaFeast.model.User;

public interface UserService extends UserDetailsService{
	User loadByEmail(final String email);
	
	List<User> loadAll();
	
	List<User> loadAllByRole(final String name);
}
