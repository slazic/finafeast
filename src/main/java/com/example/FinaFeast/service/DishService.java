package com.example.FinaFeast.service;

import java.util.List;

import com.example.FinaFeast.model.Dish;

public interface DishService {
	Dish loadById(final Long id);
	
	List<Dish> loadAll();
	
	List<Dish> loadAllByOrderId(final Long orderId);
	
	void create(final Dish t);
	
	void update(final Dish t);
	
	void delete(final Long id);
}
